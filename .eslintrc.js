module.exports = {
    "extends": ["react-app", "prettier"],
    rules: {
        quotes: ["error", "backtick"],
        "prettier/prettier": "error"
    },
    "plugins": ["prettier"]
};