const path = require(`path`);

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  return new Promise((resolve, reject) => {
    graphql(`
      {
        allDirectusFlexboxItem {
          edges {
            node {
              slug
              directusId
            }
          }
        }
      }
    `).then(results => {
      results.data.allDirectusFlexboxItem.edges.forEach(({ node }) => {
        createPage({
          path: `/examples/${node.slug}`,
          component: path.resolve(`./src/components/ExampleItem.js`),
          context: {
            id: node.directusId
          }
        });
      });
      resolve();
    });
  });
};
