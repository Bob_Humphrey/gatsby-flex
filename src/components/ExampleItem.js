import React from "react";
import { useEffect } from "react";
import Prism from "prismjs";
import { graphql } from "gatsby";
import Layout from "./layout";
import SEO from "./seo";

export const query = graphql`
  query ExampleQuery($id: Int!) {
    directusFlexboxItem(directusId: { eq: $id }) {
      name
      slug
      category
      directusId
      code
    }
  }
`;

const ExampleItem = props => {
  const code = props.data.directusFlexboxItem.code;
  useEffect(() => {
    // call the highlightAll() function to style our code blocks
    Prism.highlightAll();
  });
  return (
    <Layout>
      <SEO
        keywords={[`tailwind css`, `flexbox`]}
        title="Tailwind CSS Flexbox Cheat Sheet"
      />
      <article className="flex justify-center w-full mb-6">
        <div className="w-11/12 lg:w-3/4">
          <div className="flex justify-center font-bold text-3xl text-secondary mb-4">
            {props.data.directusFlexboxItem.name}
          </div>

          <div className="code-container">
            <pre>
              <code className="language-markup">{code}</code>
            </pre>
          </div>
        </div>
      </article>
    </Layout>
  );
};

export default ExampleItem;
