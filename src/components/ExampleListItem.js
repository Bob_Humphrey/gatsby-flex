import { Link } from "gatsby";
import React from "react";

const ExampleItemList = ({ node }) => {
  return (
    <div className="">
      <li className="w-full">
        <Link
          className="text-xl text-primary hover:text-black"
          to={`/examples/` + node.slug}
        >
          {node.name}
        </Link>
      </li>
    </div>
  );
};

export default ExampleItemList;
