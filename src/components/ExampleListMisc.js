import React from "react";
import { graphql, StaticQuery } from "gatsby";
import ExampleListItem from "./ExampleListItem";

export const COLUMN_LIST = graphql`
  query MyQueryMisc {
    allDirectusFlexboxItem(
      filter: { category: { eq: "Misc" } }
      sort: { fields: name }
    ) {
      edges {
        node {
          name
          slug
          category
          directusId
          code
        }
      }
    }
  }
`;

const ExampleListColumns = () => (
  <ul className="mb-8">
    <StaticQuery
      query={COLUMN_LIST}
      render={({ allDirectusFlexboxItem }) =>
        allDirectusFlexboxItem.edges.map(({ node }) => (
          <ExampleListItem node={node} key={node.directusId} />
        ))
      }
    />
  </ul>
);

export default ExampleListColumns;
