import React from "react";
import Links from "./Links";
import Notes from "./Notes";
import ExampleListColumns from "./ExampleListColumns";
import ExampleListForms from "./ExampleListForms";
import ExampleListMisc from "./ExampleListMisc";

const Home = () => {
  return (
    <div className="lg:flex justify-center w-full mb-6 px-10">
      <div className="lg:flex w-11/12 lg:w-3/4 xl:w-1/2">
        <div className="w-full lg:w-1/2 lg:px-20 lg:border-r lg:border-gray-400">
          <Notes />
        </div>
        <div className="w-full lg:w-1/2 lg:px-20">
          <Links />
          <h2 className="text-2xl font-bold text-secondary mb-2">Columns</h2>
          <ExampleListColumns />
          <h2 className="text-2xl font-bold text-secondary mb-2">Forms</h2>
          <ExampleListForms />
          <h2 className="text-2xl font-bold text-secondary mb-2">Misc</h2>
          <ExampleListMisc />
        </div>
      </div>
    </div>
  );
};
export default Home;
