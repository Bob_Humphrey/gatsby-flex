import React from "react";

const Links = () => {
  return (
    <div className="mb-8">
      <h2 className="text-2xl text-secondary font-bold mb-2">Links</h2>
      <ul className="">
        <li className="w-full">
          <a
            className="font-subheading text-xl text-primary hover:text-black"
            href="https://tailwindcss.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Tailwind CSS
          </a>
        </li>
        <li className="w-full">
          <a
            className="font-subheading text-xl text-primary hover:text-black"
            href="https://nerdcave.com/tailwind-cheat-sheet"
            target="_blank"
            rel="noopener noreferrer"
          >
            Tailwind Cheat Sheet
          </a>
        </li>
        <li className="w-full">
          <a
            className="font-subheading text-xl text-primary hover:text-black"
            href="https://yoksel.github.io/flex-cheatsheet/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Yoksel Flexbox Cheat Sheet
          </a>
        </li>
        <li className="w-full">
          <a
            className="font-subheading text-xl text-primary hover:text-black"
            href="https://philipwalton.github.io/solved-by-flexbox/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Solved by Flexbox
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Links;
