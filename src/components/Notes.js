import React from "react";

const Notes = () => {
  return (
    <div className="mb-8 ">
      <h2 className="text-2xl text-secondary font-bold mb-2">Horizontal</h2>
      <ul className="mb-8">
        <li className="w-full font-subheading text-xl">justify-start</li>
        <li className="w-full font-subheading text-xl">justify-center</li>
        <li className="w-full font-subheading text-xl">justify-end</li>
        <li className="w-full font-subheading text-xl">justify-between</li>
        <li className="w-full font-subheading text-xl">justify-around</li>
      </ul>
      <h2 className="text-2xl text-secondary font-bold mb-2">Vertical</h2>
      <ul className="mb-8">
        <li className="w-full font-subheading text-xl">items-stretch</li>
        <li className="w-full font-subheading text-xl">items-start</li>
        <li className="w-full font-subheading text-xl">items-center</li>
        <li className="w-full font-subheading text-xl">items-end</li>
        <li className="w-full font-subheading text-xl">items-baseline</li>
      </ul>
      <h2 className="text-2xl text-secondary font-bold mb-2">Multi Line</h2>
      <ul className="mb-8">
        <li className="w-full font-subheading text-xl">flex-wrap</li>
        <li className="w-full font-subheading text-xl">flex-no-wrap</li>
      </ul>
      <h2 className="text-2xl text-secondary font-bold mb-2">
        Align Rows Vertically
      </h2>
      <ul className="mb-8">
        <li className="w-full font-subheading text-xl">content-start</li>
        <li className="w-full font-subheading text-xl">content-center</li>
        <li className="w-full font-subheading text-xl">content-end</li>
        <li className="w-full font-subheading text-xl">content-between</li>
        <li className="w-full font-subheading text-xl">content-around</li>
      </ul>
    </div>
  );
};

export default Notes;
