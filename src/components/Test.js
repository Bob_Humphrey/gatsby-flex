import React from "react";
import { useEffect } from "react";
import Prism from "prismjs";

// The code we will be displaying
const code = `const foo = 'foo';
const bar = 'bar';
console.log(foo + bar);
`;

const Home = () => {
  useEffect(() => {
    // call the highlightAll() function to style our code blocks
    Prism.highlightAll();
  });
  return (
    <div>
            
      <div className="flex justify-center w-full mt-10 mb-6">
        <div className="w-11/12 lg:w-2/3">
            <div className="flex flex-wrap -mx-2">TEST</div>
          <h2>
            Using Prism to style <code>code blocks</code> in Gatsby
          </h2>
          <div className="code-container">
            <pre>
              <code className="language-javascript">{code}</code>
            </pre>
          </div>
        </div>
                
      </div>
            
    </div>
  );
};
export default Home;
