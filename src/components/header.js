import { Link } from "gatsby";
import React from "react";

const Header = () => {
  return (
    <header className="md:flex justify-center pt-2 md:pt-8 pb-12">
      <div className="md:flex  w-11/12 lg:w-1/2">
        <div className="w-full text-center text-primary font-bold text-4xl hover:text-black">
          <Link to={`/`}>Tailwind CSS Flexbox Cheat Sheet</Link>
        </div>
      </div>
    </header>
  );
};

export default Header;
