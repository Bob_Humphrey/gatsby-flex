import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Home from "../components/Home";

function IndexPage() {
  return (
    <Layout>
      <SEO
        keywords={[`tailwind css`, `flexbox`]}
        title="Tailwind CSS Flexbox Cheat Sheet"
      />
      <Home />
    </Layout>
  );
}

export default IndexPage;
